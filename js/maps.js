app.Maps = {
    removeDrawInteractions : function(map) {
        map.getInteractions().forEach(function(interaction) {
            if (interaction instanceof ol.interaction.Draw || interaction instanceof ol.interaction.Select || interaction instanceof app.Drag) {
                map.removeInteraction(interaction);
            }
        });
    }
}