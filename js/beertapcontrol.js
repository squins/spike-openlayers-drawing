/**
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object=} opt_options Control options.
 */
app.BeerTapControl = function(opt_options) {

    var objectStyle = new ol.style.Style({
        image: new ol.style.Icon({
            src: "images/beer.png"
        })
    });

    var options = opt_options || {};

    var button = document.createElement('button');
    button.setAttribute('title', 'Biertap');
    button.innerHTML = '<img src="images/beertap.png"/>';

    var this_ = this;
    var handleDraw = function() {

        var map = this_.getMap();

        app.Maps.removeDrawInteractions(map);

        drawInteraction = new ol.interaction.Draw({
            features: features,
            type:  'Point',
            maxPoints: 1,
            style: objectStyle
        });

        drawInteraction.on('drawend', function(drawEvent) {
            drawEvent.feature.setStyle(objectStyle);
        });

        map.addInteraction(drawInteraction);
    };

    button.addEventListener('click', handleDraw, false);
    button.addEventListener('touchstart', handleDraw, false);

    var element = document.createElement('div');
    element.className = 'beer-tap-control ol-unselectable ol-control';
    element.appendChild(button);

    ol.control.Control.call(this, {
        element: element,
        target: options.target
    });

};
ol.inherits(app.BeerTapControl, ol.control.Control);
