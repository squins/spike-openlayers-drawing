/**
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object=} opt_options Control options.
 */
app.TerrainControl = function(opt_options) {

    var options = opt_options || {};

    var button = document.createElement('button');
    button.setAttribute('title', 'Terreinmarkering');
    button.innerHTML = 'T';

    var this_ = this;
    var handleDraw = function() {

        var objectStyle =   new ol.style.Style({
            fill: new ol.style.Fill({
                color: 'rgba(0, 0, 0, 0.1)'
            }),
            stroke: new ol.style.Stroke({
                color: '#ffcc33',
                width: 2
            })
        });

        var map = this_.getMap();

        app.Maps.removeDrawInteractions(map);

        drawInteraction = new ol.interaction.Draw({
            features: features,
            type:  'Polygon',
            style: objectStyle
        });


        drawInteraction.on('drawend', function(drawEvent) {
            drawEvent.feature.setStyle(objectStyle);
        });

        map.addInteraction(drawInteraction);
    };

    button.addEventListener('click', handleDraw, false);
    button.addEventListener('touchstart', handleDraw, false);

    var element = document.createElement('div');
    element.className = 'terrain-control ol-unselectable ol-control';
    element.appendChild(button);

    ol.control.Control.call(this, {
        element: element,
        target: options.target
    });

};
ol.inherits(app.TerrainControl, ol.control.Control);
