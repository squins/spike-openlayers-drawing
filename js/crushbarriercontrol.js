/**
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object=} opt_options Control options.
 */
app.CrushBarrierControl = function(opt_options) {


    var objectStyle =   new ol.style.Style({
        fill: new ol.style.Fill({
            color: 'rgba(0, 0, 0, 0.2)'
        }),
        stroke: new ol.style.Stroke({
            color: 'gray',
            lineDash: [9, 9],
            width: 5
        })
    });



    var options = opt_options || {};

    var button = document.createElement('button');
    button.setAttribute('title', 'Dranghekken');
    button.innerHTML = '<img src="images/crushbarrier-icon.png"/>';

    var this_ = this;
    var handleDraw = function() {

        var map = this_.getMap();

        app.Maps.removeDrawInteractions(map);

        drawInteraction = new ol.interaction.Draw({
            features: features,
            type:  'LineString',
            maxPoints: 2,
            style: objectStyle
        });

        drawInteraction.on('drawend', function(drawEvent) {
            drawEvent.feature.setStyle(objectStyle);
        });


        map.addInteraction(drawInteraction);
    };

    button.addEventListener('click', handleDraw, false);
    button.addEventListener('touchstart', handleDraw, false);

    var element = document.createElement('div');
    element.className = 'crush-barrier-control ol-unselectable ol-control';
    element.appendChild(button);

    ol.control.Control.call(this, {
        element: element,
        target: options.target
    });

};
ol.inherits(app.CrushBarrierControl, ol.control.Control);
