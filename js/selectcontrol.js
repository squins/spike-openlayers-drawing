/**
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object=} opt_options Control options.
 */
app.SelectControl = function(opt_options) {
    var options = opt_options || {};

    var button = document.createElement('button');
    button.setAttribute('title', 'Selecteren');
    button.innerHTML = '<img src="images/select.png"/>';

    var this_ = this;
    var handleSelect = function() {
        var map = this_.getMap();
        app.Maps.removeDrawInteractions(map);
        map.addInteraction(new ol.interaction.Select());
    };

    button.addEventListener('click', handleSelect, false);
    button.addEventListener('touchstart', handleSelect, false);

    var element = document.createElement('div');
    element.className = 'select-control ol-unselectable ol-control';
    element.appendChild(button);

    ol.control.Control.call(this, {
        element: element,
        target: options.target
    });

};
ol.inherits(app.SelectControl, ol.control.Control);
